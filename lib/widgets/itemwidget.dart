import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobxshoppingcart/models/item.dart';

class ItemWidget extends StatelessWidget {
  final ItemModel itemModel;
  final Function removeItem;
  final Function editDetail;

  const ItemWidget({this.itemModel, this.removeItem, this.editDetail});

  @override
  Widget build(BuildContext context) {
    return Observer(
        builder: (_) => ListTile(
              title: Wrap(
                children: <Widget>[
                  Text(itemModel.title, style: TextStyle(fontSize: 20),)
                ],
              ),
              leading: Checkbox(
                value: itemModel.check,
                onChanged: (bool val) {
                  itemModel.setCheck(val);
                },
              ),
              trailing: Wrap(
                children: <Widget>[
                  IconButton(
                    color: Colors.pinkAccent,
                    icon: Icon(Icons.edit),
                    onPressed: editDetail,
                  ),
                  IconButton(
                    color: Colors.red,
                    icon: Icon(Icons.remove_circle),
                    onPressed: removeItem,
                  ),
                ],
              ),
            ));
  }
}
