import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobxshoppingcart/models/detail.dart';
import 'package:mobxshoppingcart/models/item.dart';

class ItemUpdateWidget extends StatelessWidget {
  final Detail detail;
  final Function editDetail;

  const ItemUpdateWidget({this.detail, this.editDetail});

  @override
  Widget build(BuildContext context) {
    return Observer(
        builder: (_) => ListTile(
              title: Wrap(
                children: <Widget>[
                  Text('${detail.id} - '),
                  Text('details: ${detail.detail}')
                ],
              ),
              trailing: Wrap(
                children: <Widget>[
                  IconButton(
                    color: Colors.pinkAccent,
                    icon: Icon(Icons.edit),
                    onPressed: editDetail,
                  ),
                ],
              ),
            ));
  }
}
