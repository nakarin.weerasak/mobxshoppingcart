import 'package:mobx/mobx.dart';
import 'package:mobxshoppingcart/models/detail.dart';
import 'package:mobxshoppingcart/models/item.dart';
part 'homecontroller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  @observable
  ObservableList<ItemModel> listItems = ObservableList<ItemModel>();

  @action
  addItem(ItemModel _item) {
    listItems.add(_item);
  }

  @action
  removeItem(ItemModel _item) {
    listItems.removeWhere((ItemModel item) => item.title == _item.title);
  }

  @action
  editDetail(ItemModel _itemSelect, Detail _detail) {
    int indexItem = listItems.indexWhere((item) => item.title == _itemSelect.title);
    int indexDetail = listItems[indexItem].details.indexWhere((detail) => detail.id == _detail.id);
    listItems[indexItem].details[indexDetail] = _detail;
  }

  @computed
  int get totolItemCheck =>
      listItems.where((item) => item.check == true).length;
}
