// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Detail on _DetailBase, Store {
  final _$idAtom = Atom(name: '_DetailBase.id');

  @override
  int get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$detailAtom = Atom(name: '_DetailBase.detail');

  @override
  String get detail {
    _$detailAtom.context.enforceReadPolicy(_$detailAtom);
    _$detailAtom.reportObserved();
    return super.detail;
  }

  @override
  set detail(String value) {
    _$detailAtom.context.conditionallyRunInAction(() {
      super.detail = value;
      _$detailAtom.reportChanged();
    }, _$detailAtom, name: '${_$detailAtom.name}_set');
  }

  @override
  String toString() {
    final string = 'id: ${id.toString()},detail: ${detail.toString()}';
    return '{$string}';
  }
}
