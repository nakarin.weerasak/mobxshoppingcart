import 'package:mobx/mobx.dart';
import 'package:mobxshoppingcart/models/detail.dart';
part 'item.g.dart';

class ItemModel = _ItemModelBase with _$ItemModel;

abstract class _ItemModelBase with Store {

  @observable
  String title;

  @observable
  bool check;

  @observable
  ObservableList<Detail> details = ObservableList<Detail>();

  @action
  setTitle(String _title) => title = _title;

  @action
  setCheck(bool _check) => check = _check;

  _ItemModelBase({this.title, this.check, this.details});

}