import 'package:mobx/mobx.dart';
part 'detail.g.dart';

class Detail = _DetailBase with _$Detail;

abstract class _DetailBase with Store {
  @observable
  int id;

  @observable
  String detail;

  _DetailBase({this.id, this.detail});
}