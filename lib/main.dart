import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:mobxshoppingcart/controllers/homecontroller.dart';
import 'package:mobxshoppingcart/models/detail.dart';
import 'package:mobxshoppingcart/models/item.dart';
import 'package:mobxshoppingcart/widgets/itemupdatewidget.dart';
import 'package:mobxshoppingcart/widgets/itemwidget.dart';

void main() {
  GetIt getIt = GetIt.I;
  getIt.registerSingleton<HomeController>(HomeController());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shopping Cart',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Shopping Cart'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  final homeController = GetIt.I.get<HomeController>();
  _dialogAdd() {
    var itemModel = ItemModel();

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Center(
              child: Text('Insert'),
            ),
            content: TextField(
              onChanged: (value) {
                itemModel.title = value;
              },
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Please Inser Item'),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  itemModel.check = false;
                  itemModel.details = [
                    Detail(id: 1, detail: "first detail"),
                    Detail(id: 2, detail: "second detail")
                  ].asObservable();
                  homeController.addItem(itemModel);
                  Navigator.pop(context);
                },
                child: Text("Add to Cart"),
              )
            ],
          );
        });
  }

  _dialogEdit(ItemModel itemUpdate) {

    autorun((_) {
      print(homeController.listItems.length);
    });

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Center(
              child: Text('Update Details ${itemUpdate.title}'),
            ),
            content: Container(
              width: double.maxFinite,
              child: Observer(
                  builder: (_) => ListView.separated(
                        separatorBuilder: (context, index) => Divider(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) => ItemUpdateWidget(
                          detail: itemUpdate.details[index],
                          editDetail: () {
                            Detail detailUpdate = Detail(
                                id: itemUpdate.details[index].id,
                                detail: DateTime.now().millisecond.toString());
                            homeController.editDetail(itemUpdate, detailUpdate);
                          },
                        ),
                        itemCount: itemUpdate.details.length,
                      )),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("OK"),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.title)),
        actions: <Widget>[
          new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Container(
                  height: 150.0,
                  width: 30.0,
                  child: new GestureDetector(
                      onTap: () {},
                      child: new Stack(
                        children: <Widget>[
                          new IconButton(
                            icon: new Icon(
                              Icons.shopping_cart,
                              color: Colors.white,
                            ),
                            onPressed: null,
                          ),
                          new Positioned(
                              child: new Stack(
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: 20.0, color: Colors.red),
                              new Positioned(
                                  top: 3.0,
                                  right: 4.0,
                                  child: new Center(
                                    child: new Observer(
                                        builder: (context) => Text(
                                            homeController.totolItemCheck
                                                .toString())),
                                  )),
                            ],
                          )),
                        ],
                      ))))
        ],
      ),

      body: Observer(builder: (_) {
        if (homeController.listItems.length > 0) {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemBuilder: (context, index) {
              ItemModel item = homeController.listItems[index];
              return ItemWidget(
                itemModel: item,
                removeItem: () {
                  homeController.removeItem(item);
                },
                editDetail: () {
                  _dialogEdit(item);
                },
              );
            },
            itemCount: homeController.listItems.length,
          );
        } else {
          return Center(
            child: Text(
              "No Data",
              style: TextStyle(fontSize: 20),
            ),
          );
        }
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: _dialogAdd,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
